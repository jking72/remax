<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function getPlayers() {

        $players = DB::table('players')
            ->orderBy('priority', 'asc')
            ->get();

        return response()->json(
            $players, 200);
    }

    public function addPlayer(Request $request)
    {

        $player = new Player;
        $player->fname = $request->fname;
        $player->lname = $request->lname;
        $player->hometown = $request->hometown;
        $player->year = $request->year;
        $player->pts = $request->pts;
        $player->ppg = $request->ppg;
        $player->rpg = $request->rpg;
        $player->tpm = $request->tpm;
        $player->tpa = $request->tpa;
        $player->priority = $player::max('priority') + 1;

        $player->save();
        $newId = $player->id;

        return response()->json([
            "message" => $newId
        ], 201);

    }

    public function deletePlayer($id) {

        $player = Player::find($id);
        $player->delete();

        return response()->json([
            "message" => "player record deleted"
        ], 201);

    }

    public function changeOrder(Request $request)
    {
        $error = false;

        $postData = $request->input();

        for ($i = 0; $i < count($postData); $i++) {
            $tmpResponse = Player::where('id', $postData[$i]['id'])
                ->update(['priority' => $postData[$i]['order']]);
            if (!$tmpResponse) {
                $error = true;
            }
        }

        return response()->json([
            "message" => "order saved"
        ], 201);

    }

}
