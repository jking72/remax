<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;

class WebController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function homePage()
    {

        //$response = Http::get('http://ec2-3-13-122-204.us-east-2.compute.amazonaws.com/api/players');
        //$data = array('response' => $response);

        $view = View::make('home');
        return $view;

    }

    public function addPage()

    {
        return View::make('add');
    }

    public function addPlayer(Request $request)
    {
        parse_str($request->getContent(), $data);
        unset($data['_token']);

        $response = Http::post('http://ec2-3-13-122-204.us-east-2.compute.amazonaws.com/api/players', $data);

        return View::make('success');
    }

}
