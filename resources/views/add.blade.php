<html>
<head>
    <title>Remax Assessment</title>
    <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}" />
</head>
<body>
<h1>Remax Assessment - Add a player</h1>

<form method="post" name="addForm" action="/add">
    @csrf
    <ul>
        <li>First name: <input type="text" name="fname"></li>
        <li>Last name: <input type="text" name="lname"></li>
        <li>Hometown: <input type="text" name="hometown"></li>
        <li>Year: <input type="text" name="year"></li>
        <li>Pts: <input type="text" name="pts"></li>
        <li>Ppg: <input type="text" name="ppg"></li>
        <li>Rpg: <input type="text" name="rpg"></li>
        <li>3pm: <input type="text" name="tpm"></li>
        <li>3pa: <input type="text" name="tpa"></li>
        <input type="submit" value="Add player">
    </ul>
</form>

<p><a href="/">All Players | </a><a href="/add">Add a player</a></p>

</body>



</html>
