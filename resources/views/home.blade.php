<html>
<head>
    <title>Remax Assessment</title>
    <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}"/>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>
<body>

    <h1>Remax Assessment</h1>

    <div id="app">
        <table-component></table-component>
    </div>

<script src="/js/app.js"></script>
</body>
</html>
