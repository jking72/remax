<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('players', 'App\Http\Controllers\ApiController@addPlayer');
Route::get('players', 'App\Http\Controllers\ApiController@getPlayers');
Route::post('order', 'App\Http\Controllers\ApiController@changeOrder');
Route::delete('players/{id}', 'App\Http\Controllers\ApiController@deletePlayer');

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
