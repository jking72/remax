<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Http\Response;

class ApiTest extends TestCase
{
    public $newId;

    public function testGetPlayers()
    {
        $response = $this->get('/api/players');
        $response->assertStatus(200);
    }

    public function testAddDeletePlayer()
    {
        $response = $this->post('/api/players', [
            'fname' => 'Jon',
            'lname' => 'King',
            'hometown' => 'Latrobe, PA',
            'year' => 'Sr',
            'pts' => 15,
            'ppg' => 2.5,
            'rpg' => 0.8,
            'tpm' => 2,
            'tpa' => 5,
            'priority' => 8
        ]);

        $tmpData = json_decode($response->getContent());
        $newId = $tmpData->message;

        $response->assertStatus(201);

        $response = $this->delete('/api/players/' . $newId);
        $response->assertStatus(201);

    }

    public function testChangeOrder()
    {
        $response = $this->post('/api/order', [
            array('id' => 1, 'order' => 7),
            array('id' => 2, 'order' => 6),
            array('id' => 3, 'order' => 5),
            array('id' => 4, 'order' => 4),
            array('id' => 5, 'order' => 3),
            array('id' => 6, 'order' => 2),
            array('id' => 7, 'order' => 1),
        ]);

        $response->assertStatus(201);
    }
}
