<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Http\Response;

class PlayerTest extends TestCase
{
    public function testHome()
    {
        $response = $this->call('GET', '/');
        $response->assertStatus(200);
    }

    public function testAddPage()
    {
        $response = $this->call('GET', '/add');
        $response->assertStatus(200);
    }

    public function testAddPlayer()
    {
        $postData = [
            'fname' => 'Jon',
            'lname' => 'King',
            'hometown' => 'Latrobe, PA',
            'year' => 'Senior',
            'pts' => '15',
            'ppg' => '2.4',
            'rpg' => '0.5',
            'tpm' => '2',
            'tpa' => '5',
            'priority' => 8
        ];

        $response = $this->post('/add', $postData);
        $response->assertStatus(200);

    }


}
